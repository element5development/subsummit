<?php 
/*----------------------------------------------------------------*\

	DEFAULT CATEGORY ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php // Find current date and time
	$universal_date_now = date('Y-m-d H:i:s');
	$universal_time_now = strtotime($universal_date_now);
	$detroit_time_now = strtotime('-5 hours', $universal_time_now);
	$detroit_date_now = date('Y-m-d H:i:s', $detroit_time_now);
	$detroit_date_now_value = date('YmdHis', $detroit_time_now);
	$future_time_now = strtotime('-4 hours', $universal_time_now);
	$future_date_now = date('Y-m-d H:i:s', $future_time_now);
	$future_date_now_value = date('YmdHis', $future_time_now);
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head is-wide">
	<?php //taxonomy
		$taxonomies = get_terms( array(
			'taxonomy' => 'room',
			'hide_empty' => true
		) );
		$page_id = $wp_query->get_queried_object_id();
	?>
	<nav>
		<ul>
			<li>
				<a href="/watch-now">All Rooms</a>
			</li>
			<?php foreach( $taxonomies as $category ) : ?>
				<?php
					if ( $page_id == $category->term_id ) :
						$class = "is-active";	
					else :
						$class = " ";
					endif;
				?>
				<li>
					<a href="<?php echo get_category_link( $category->term_id ); ?>" class="<?php echo $class; ?>"><?php echo $category->name ?></a>
				</li>
			<?php endforeach; ?>
		</ul>
	</nav>
</header>

<main id="main-content">
	<section class="room-grid is-wide">
		<?php //ROOM QUERY
			$args_room = array(
				'posts_per_page' => -1,
				'post_type'      => 'live',
				'tax_query' => array(
					array (
							'taxonomy' => 'room',
							'field' => 'id',
							'terms' => $page_id,
					)
				),
				'meta_query'     => array(
					array(
							'key'         => 'end_time',
							'compare'     => '>',
							'value'       => $detroit_date_now,
							'type'        => 'DATETIME'
					)
				),
				'order'          => 'ASC',
				'orderby'        => 'meta_value',
				'meta_key'       => 'start_time',
				'meta_type'      => 'DATETIME',
			);
			$room = new WP_Query( $args_room );
		?>
		<?php $i = 0; while ( $room->have_posts() ) : $room->the_post(); $i++;?>
			<article>
				<div class="video-wrapper">
					<?php the_field('video_id'); ?>
				</div>
				<?php if ( $i > 1 ) : ?>
					<?php $session_start_time = date("l g:i A", strtotime(get_field('start_time')));  ?>
					<p><?php echo $session_start_time; ?></p>
				<?php endif; ?>
			</article>
		<?php endwhile; ?>
	</section>
</main>

<aside class="subta-cta">
	<div class="is-wide">
		<div class="poster"></div>
		<div>
			<h2><span>New Series Unlocked</span>Picking the Perfect<br/>Subscription E-Commerce Platform</h2>
			<p>SUBTA is an ever-growing community and by being part of you will gain even more  Exclusive Discounts, Member only Articles, Videos, and Events like this.</p>
			<a target="_blank" href="https://subta.com/show/picking-the-perfect-platform-2021/" class="button is-pink">Watch Now</a>
		</div>
	</div>
</aside>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>