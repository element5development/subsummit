<?php 
/*----------------------------------------------------------------*\

	Template Name: Schedule

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/cookie-bar'); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<main id="main-content">
	<article>

		<nav class="tabs is-narrow">
			<?php if( have_rows('schedule') ): ?>
				<button class="day1 is-ghost is-active"><?php the_field('schedule_title'); ?></button>
			<?php endif; ?>
			<?php if( have_rows('schedule_2') ): ?>
				<button class="day2 is-ghost"><?php the_field('schedule_title_2'); ?></button>
			<?php endif; ?>
			<?php if( have_rows('schedule_3') ): ?>
				<button class="day3 is-ghost"><?php the_field('schedule_title_3'); ?></button>
			<?php endif; ?>
		</nav>

		<?php if( have_rows('schedule') ): ?>
			<section id="schedule" class="schedule day1 is-active two-tracks <?php if ( get_field('hide_time') ): ?>time-hidden<?php endif; ?>">
				<h2><?php the_field('schedule_title'); ?></h2>
				<div class="schedule-grid">
					<?php if ( get_field('track_title') || get_field('track2_title') ) : ?>
						<div class="item labels">
							<?php if ( !get_field('hide_time') ) : ?>
								<div class="time"></div>
							<?php endif; ?>
							<?php if ( get_field('track_title') ): ?>
								<div class="description track1">
									<h3><?php the_field('track_title'); ?></h3>
									<?php if ( get_field('track_description') ): ?>
										<p><?php the_field('track_description'); ?></p>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track2_title') ): ?>
								<div class="description track2">
									<h3><?php the_field('track2_title'); ?></h3>
									<?php if ( get_field('track2_description') ): ?>
										<p><?php the_field('track2_description'); ?></p>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track3_title') ): ?>
								<div class="description track3">
									<h3><?php the_field('track3_title'); ?></h3>
									<?php if ( get_field('track3_description') ): ?>
										<p><?php the_field('track3_description'); ?></p>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track4_title') ): ?>
								<div class="description track4">
									<h3><?php the_field('track4_title'); ?></h3>
									<?php if ( get_field('track4_description') ): ?>
										<p><?php the_field('track4_description'); ?></p>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track5_title') ): ?>
								<div class="description track5">
									<h3><?php the_field('track5_title'); ?></h3>
									<?php if ( get_field('track5_description') ): ?>
										<p><?php the_field('track5_description'); ?></p>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					<?php while ( have_rows('schedule') ) : the_row(); ?>
						<div class="item">
							<?php if ( get_sub_field('time') && !get_field('hide_time') ): ?>
								<div class="time"><?php the_sub_field('time'); ?></div>
							<?php endif; ?>
							<?php if ( get_sub_field('title') ): ?>
								<div class="description track1">
									<h3>
										<?php if ( get_sub_field('hide_track_01_title') ): ?><?php else: ?><span><?php the_field('track_title'); ?></span><?php endif; ?>
										<?php the_sub_field('title'); ?>
									</h3>
									<?php if ( get_sub_field('sponsor') ): ?>
										<?php $featured_posts = get_sub_field('sponsor'); ?>
										<?php foreach( $featured_posts as $featured_post ): ?>
											<div class="sponsor">
												<p>Presented By</p>
												<?php $image = get_field( 'logo', $featured_post->ID ); ?>
												<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
											</div>
										<?php endforeach; ?>
									<?php endif; ?>
									<?php if ( get_sub_field('description') ): ?>
										<p><?php the_sub_field('description'); ?></p>
									<?php endif; ?>
									<?php $speaker_posts = get_sub_field('speaker_list'); ?>
									<?php if( $speaker_posts ): ?>
										<ul>
											<?php foreach( $speaker_posts as $post ): ?>
												<?php setup_postdata($post); ?>
												<li>
													<?php $image = get_field('headshot'); ?>
													<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($alt); ?>" />
													<div>
														<p class="name"><?php the_title(); ?></p>
														<p class="company"><?php the_field('company_name'); ?></p>
													</div>
												</li>
											<?php endforeach; ?>
										</ul>
										<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track2_title') && get_sub_field('title_2') ): ?>
								<div class="description track2">
									<?php if ( get_sub_field('title_2') ): ?>
										<h3>
											<?php if ( get_sub_field('hide_track_02_title') ): ?><?php else: ?><span><?php the_field('track2_title'); ?></span><?php endif; ?>
											<?php the_sub_field('title_2'); ?>
										</h3>
									<?php endif; ?>
									<?php if ( get_sub_field('description_2') ): ?>
										<p><?php the_sub_field('description_2'); ?></p>
									<?php endif; ?>
									<?php $speaker_posts = get_sub_field('speaker_list_2'); ?>
									<?php if( $speaker_posts ): ?>
										<ul>
											<?php foreach( $speaker_posts as $post ): ?>
												<?php setup_postdata($post); ?>
												<li>
													<?php $image = get_field('headshot'); ?>
													<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($alt); ?>" />
													<div>
														<p class="name"><?php the_title(); ?></p>
														<p class="company"><?php the_field('company_name'); ?></p>
													</div>
												</li>
											<?php endforeach; ?>
										</ul>
										<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track3_title') && get_sub_field('title_3') ): ?>
								<div class="description track3">
									<?php if ( get_sub_field('title_3') ): ?>
										<h3>
											<?php if ( get_sub_field('hide_track_03_title') ): ?><?php else: ?><span><?php the_field('track3_title'); ?></span><?php endif; ?>
											<?php the_sub_field('title_3'); ?>
										</h3>
									<?php endif; ?>
									<?php if ( get_sub_field('description_3') ): ?>
										<p><?php the_sub_field('description_3'); ?></p>
									<?php endif; ?>
									<?php $speaker_posts = get_sub_field('speaker_list_3'); ?>
									<?php if( $speaker_posts ): ?>
										<ul>
											<?php foreach( $speaker_posts as $post ): ?>
												<?php setup_postdata($post); ?>
												<li>
													<?php $image = get_field('headshot'); ?>
													<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($alt); ?>" />
													<div>
														<p class="name"><?php the_title(); ?></p>
														<p class="company"><?php the_field('company_name'); ?></p>
													</div>
												</li>
											<?php endforeach; ?>
										</ul>
										<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track4_title') && get_sub_field('title_4') ): ?>
								<div class="description track4">
									<?php if ( get_sub_field('title_4') ): ?>
										<h3>
											<?php if ( get_sub_field('hide_track_04_title') ): ?><?php else: ?><span><?php the_field('track4_title'); ?></span><?php endif; ?>
											<?php the_sub_field('title_4'); ?>
										</h3>
									<?php endif; ?>
									<?php if ( get_sub_field('description_4') ): ?>
										<p><?php the_sub_field('description_4'); ?></p>
									<?php endif; ?>
									<?php $speaker_posts = get_sub_field('speaker_list_4'); ?>
									<?php if( $speaker_posts ): ?>
										<ul>
											<?php foreach( $speaker_posts as $post ): ?>
												<?php setup_postdata($post); ?>
												<li>
													<?php $image = get_field('headshot'); ?>
													<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($alt); ?>" />
													<div>
														<p class="name"><?php the_title(); ?></p>
														<p class="company"><?php the_field('company_name'); ?></p>
													</div>
												</li>
											<?php endforeach; ?>
										</ul>
										<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track5_title') && get_sub_field('title_5') ): ?>
								<div class="description track5">
									<?php if ( get_sub_field('title_5') ): ?>
										<h3>
											<?php if ( get_sub_field('hide_track_05_title') ): ?><?php else: ?><span><?php the_field('track5_title'); ?></span><?php endif; ?>
											<?php the_sub_field('title_5'); ?>
										</h3>
									<?php endif; ?>
									<?php if ( get_sub_field('description_5') ): ?>
										<p><?php the_sub_field('description_5'); ?></p>
									<?php endif; ?>
									<?php $speaker_posts = get_sub_field('speaker_list_5'); ?>
									<?php if( $speaker_posts ): ?>
										<ul>
											<?php foreach( $speaker_posts as $post ): ?>
												<?php setup_postdata($post); ?>
												<li>
													<?php $image = get_field('headshot'); ?>
													<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($alt); ?>" />
													<div>
														<p class="name"><?php the_title(); ?></p>
														<p class="company"><?php the_field('company_name'); ?></p>
													</div>
												</li>
											<?php endforeach; ?>
										</ul>
										<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>
		<?php if( have_rows('schedule_2') ): ?>
			<section id="schedule" class="schedule day2 two-tracks <?php if ( get_field('hide_time_2') ): ?>time-hidden<?php endif; ?>">
				<h2><?php the_field('schedule_title_2'); ?></h2>
				<div class="schedule-grid">
					<?php if ( get_field('track_title_2') || get_field('track2_title_2') ) : ?>
						<div class="item labels">
							<?php if ( !get_field('hide_time_2') ) : ?>
								<div class="time"></div>
							<?php endif; ?>
							<?php if ( get_field('track_title_2') ): ?>
								<div class="description track1">
									<h3><?php the_field('track_title_2'); ?></h3>
									<?php if ( get_field('track_description_2') ): ?>
										<p><?php the_field('track_description_2'); ?></p>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track2_title_2') ): ?>
								<div class="description track2">
									<h3><?php the_field('track2_title_2'); ?></h3>
									<?php if ( get_field('track2_description_2') ): ?>
										<p><?php the_field('track2_description_2'); ?></p>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track3_title_2') ): ?>
								<div class="description track3">
									<h3><?php the_field('track3_title_2'); ?></h3>
									<?php if ( get_field('track3_description_2') ): ?>
										<p><?php the_field('track3_description_2'); ?></p>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track4_title_2') ): ?>
								<div class="description track4">
									<h3><?php the_field('track4_title_2'); ?></h3>
									<?php if ( get_field('track4_description_2') ): ?>
										<p><?php the_field('track4_description_2'); ?></p>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track5_title_2') ): ?>
								<div class="description track5">
									<h3><?php the_field('track5_title_2'); ?></h3>
									<?php if ( get_field('track5_description_2') ): ?>
										<p><?php the_field('track5_description_2'); ?></p>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					<?php while ( have_rows('schedule_2') ) : the_row(); ?>
						<div class="item">
							<?php if ( get_sub_field('time') && !get_field('hide_time_2') ): ?>
								<div class="time"><?php the_sub_field('time'); ?></div>
							<?php endif; ?>
							<?php if ( get_sub_field('title') ): ?>
								<div class="description track1">
									<h3>
										<?php if ( get_sub_field('hide_track_01_title') ): ?><?php else: ?><span><?php the_field('track_title_2'); ?></span><?php endif; ?>
										<?php the_sub_field('title'); ?>
									</h3>
									<?php if ( get_sub_field('description') ): ?>
										<p><?php the_sub_field('description'); ?></p>
									<?php endif; ?>
									<?php $speaker_posts = get_sub_field('speaker_list'); ?>
									<?php if( $speaker_posts ): ?>
										<ul>
											<?php foreach( $speaker_posts as $post ): ?>
												<?php setup_postdata($post); ?>
												<li>
													<?php $image = get_field('headshot'); ?>
													<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($alt); ?>" />
													<div>
														<p class="name"><?php the_title(); ?></p>
														<p class="company"><?php the_field('company_name'); ?></p>
													</div>
												</li>
											<?php endforeach; ?>
										</ul>
										<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track2_title_2') && get_sub_field('title_2') ): ?>
								<div class="description track2">
									<?php if ( get_sub_field('title_2') ): ?>
										<h3>
											<?php if ( get_sub_field('hide_track_02_title') ): ?><?php else: ?><span><?php the_field('track2_title_2'); ?></span><?php endif; ?>
											<?php the_sub_field('title_2'); ?>
										</h3>
									<?php endif; ?>
									<?php if ( get_sub_field('description_2') ): ?>
										<p><?php the_sub_field('description_2'); ?></p>
									<?php endif; ?>
									<?php $speaker_posts = get_sub_field('speaker_list_2'); ?>
									<?php if( $speaker_posts ): ?>
										<ul>
											<?php foreach( $speaker_posts as $post ): ?>
												<?php setup_postdata($post); ?>
												<li>
													<?php $image = get_field('headshot'); ?>
													<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($alt); ?>" />
													<div>
														<p class="name"><?php the_title(); ?></p>
														<p class="company"><?php the_field('company_name'); ?></p>
													</div>
												</li>
											<?php endforeach; ?>
										</ul>
										<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track3_title_2') && get_sub_field('title_3') ): ?>
								<div class="description track3">
									<?php if ( get_sub_field('title_3') ): ?>
										<h3>
											<?php if ( get_sub_field('hide_track_03_title') ): ?><?php else: ?><span><?php the_field('track3_title_2'); ?></span><?php endif; ?>
											<?php the_sub_field('title_3'); ?>
										</h3>
									<?php endif; ?>
									<?php if ( get_sub_field('description_3') ): ?>
										<p><?php the_sub_field('description_3'); ?></p>
									<?php endif; ?>
									<?php $speaker_posts = get_sub_field('speaker_list_3'); ?>
									<?php if( $speaker_posts ): ?>
										<ul>
											<?php foreach( $speaker_posts as $post ): ?>
												<?php setup_postdata($post); ?>
												<li>
													<?php $image = get_field('headshot'); ?>
													<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($alt); ?>" />
													<div>
														<p class="name"><?php the_title(); ?></p>
														<p class="company"><?php the_field('company_name'); ?></p>
													</div>
												</li>
											<?php endforeach; ?>
										</ul>
										<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track4_title_2') && get_sub_field('title_4') ): ?>
								<div class="description track4">
									<?php if ( get_sub_field('title_4') ): ?>
										<h3>
											<?php if ( get_sub_field('hide_track_04_title') ): ?><?php else: ?><span><?php the_field('track4_title_2'); ?></span><?php endif; ?>
											<?php the_sub_field('title_4'); ?>
										</h3>
									<?php endif; ?>
									<?php if ( get_sub_field('description_4') ): ?>
										<p><?php the_sub_field('description_4'); ?></p>
									<?php endif; ?>
									<?php $speaker_posts = get_sub_field('speaker_list_4'); ?>
									<?php if( $speaker_posts ): ?>
										<ul>
											<?php foreach( $speaker_posts as $post ): ?>
												<?php setup_postdata($post); ?>
												<li>
													<?php $image = get_field('headshot'); ?>
													<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($alt); ?>" />
													<div>
														<p class="name"><?php the_title(); ?></p>
														<p class="company"><?php the_field('company_name'); ?></p>
													</div>
												</li>
											<?php endforeach; ?>
										</ul>
										<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track5_title_2') && get_sub_field('title_5') ): ?>
								<div class="description track5">
									<?php if ( get_sub_field('title_5') ): ?>
										<h3>
											<?php if ( get_sub_field('hide_track_05_title') ): ?><?php else: ?><span><?php the_field('track5_title_2'); ?></span><?php endif; ?>
											<?php the_sub_field('title_5'); ?>
										</h3>
									<?php endif; ?>
									<?php if ( get_sub_field('description_5') ): ?>
										<p><?php the_sub_field('description_5'); ?></p>
									<?php endif; ?>
									<?php $speaker_posts = get_sub_field('speaker_list_5'); ?>
									<?php if( $speaker_posts ): ?>
										<ul>
											<?php foreach( $speaker_posts as $post ): ?>
												<?php setup_postdata($post); ?>
												<li>
													<?php $image = get_field('headshot'); ?>
													<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($alt); ?>" />
													<div>
														<p class="name"><?php the_title(); ?></p>
														<p class="company"><?php the_field('company_name'); ?></p>
													</div>
												</li>
											<?php endforeach; ?>
										</ul>
										<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>
		<?php if( have_rows('schedule_3') ): ?>
			<section id="schedule" class="schedule day3 two-tracks <?php if ( get_field('hide_time_3') ): ?>time-hidden<?php endif; ?>">
				<h2><?php the_field('schedule_title_3'); ?></h2>
				<div class="schedule-grid">
					<?php if ( get_field('track_title_3') || get_field('track2_title_3') ) : ?>
						<div class="item labels">
							<?php if ( !get_field('hide_time_3') ) : ?>
								<div class="time"></div>
							<?php endif; ?>
							<?php if ( get_field('track_title_3') ): ?>
								<div class="description track1">
									<h3><?php the_field('track_title_3'); ?></h3>
									<?php if ( get_field('track_description_3') ): ?>
										<p><?php the_field('track_description_3'); ?></p>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track2_title_3') ): ?>
								<div class="description track2">
									<h3><?php the_field('track2_title_3'); ?></h3>
									<?php if ( get_field('track2_description_3') ): ?>
										<p><?php the_field('track2_description_3'); ?></p>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track3_title_3') ): ?>
								<div class="description track3">
									<h3><?php the_field('track3_title_3'); ?></h3>
									<?php if ( get_field('track3_description_3') ): ?>
										<p><?php the_field('track3_description_3'); ?></p>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track4_title_3') ): ?>
								<div class="description track4">
									<h3><?php the_field('track4_title_3'); ?></h3>
									<?php if ( get_field('track4_description_3') ): ?>
										<p><?php the_field('track4_description_3'); ?></p>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track5_title_3') ): ?>
								<div class="description track5">
									<h3><?php the_field('track5_title_3'); ?></h3>
									<?php if ( get_field('track5_description_3') ): ?>
										<p><?php the_field('track5_description_3'); ?></p>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					<?php while ( have_rows('schedule_3') ) : the_row(); ?>
						<div class="item">
							<?php if ( get_sub_field('time') && !get_field('hide_time_3') ): ?>
								<div class="time"><?php the_sub_field('time'); ?></div>
							<?php endif; ?>
							<?php if ( get_sub_field('title') ): ?>
								<div class="description track1">
									<h3>
										<?php if ( get_sub_field('hide_track_01_title') ): ?><?php else: ?><span><?php the_field('track_title_3'); ?></span><?php endif; ?>
										<?php the_sub_field('title'); ?>
									</h3>
									<?php if ( get_sub_field('description') ): ?>
										<p><?php the_sub_field('description'); ?></p>
									<?php endif; ?>
									<?php $speaker_posts = get_sub_field('speaker_list'); ?>
									<?php if( $speaker_posts ): ?>
										<ul>
											<?php foreach( $speaker_posts as $post ): ?>
												<?php setup_postdata($post); ?>
												<li>
													<?php $image = get_field('headshot'); ?>
													<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($alt); ?>" />
													<div>
														<p class="name"><?php the_title(); ?></p>
														<p class="company"><?php the_field('company_name'); ?></p>
													</div>
												</li>
											<?php endforeach; ?>
										</ul>
										<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track2_title_3') && get_sub_field('title_2') ): ?>
								<div class="description track2">
									<?php if ( get_sub_field('title_2') ): ?>
										<h3>
											<?php if ( get_sub_field('hide_track_02_title') ): ?><?php else: ?><span><?php the_field('track2_title_3'); ?></span><?php endif; ?>
											<?php the_sub_field('title_2'); ?>
										</h3>
									<?php endif; ?>
									<?php if ( get_sub_field('description_2') ): ?>
										<p><?php the_sub_field('description_2'); ?></p>
									<?php endif; ?>
									<?php $speaker_posts = get_sub_field('speaker_list_2'); ?>
									<?php if( $speaker_posts ): ?>
										<ul>
											<?php foreach( $speaker_posts as $post ): ?>
												<?php setup_postdata($post); ?>
												<li>
													<?php $image = get_field('headshot'); ?>
													<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($alt); ?>" />
													<div>
														<p class="name"><?php the_title(); ?></p>
														<p class="company"><?php the_field('company_name'); ?></p>
													</div>
												</li>
											<?php endforeach; ?>
										</ul>
										<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track3_title_3') && get_sub_field('title_3') ): ?>
								<div class="description track3">
									<?php if ( get_sub_field('title_3') ): ?>
										<h3>
											<?php if ( get_sub_field('hide_track_03_title') ): ?><?php else: ?><span><?php the_field('track3_title_3'); ?></span><?php endif; ?>
											<?php the_sub_field('title_3'); ?>
										</h3>
									<?php endif; ?>
									<?php if ( get_sub_field('description_3') ): ?>
										<p><?php the_sub_field('description_3'); ?></p>
									<?php endif; ?>
									<?php $speaker_posts = get_sub_field('speaker_list_3'); ?>
									<?php if( $speaker_posts ): ?>
										<ul>
											<?php foreach( $speaker_posts as $post ): ?>
												<?php setup_postdata($post); ?>
												<li>
													<?php $image = get_field('headshot'); ?>
													<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($alt); ?>" />
													<div>
														<p class="name"><?php the_title(); ?></p>
														<p class="company"><?php the_field('company_name'); ?></p>
													</div>
												</li>
											<?php endforeach; ?>
										</ul>
										<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track4_title_3') && get_sub_field('title_4') ): ?>
								<div class="description track4">
									<?php if ( get_sub_field('title_4') ): ?>
										<h3>
											<?php if ( get_sub_field('hide_track_04_title') ): ?><?php else: ?><span><?php the_field('track4_title_3'); ?></span><?php endif; ?>
											<?php the_sub_field('title_4'); ?>
										</h3>
									<?php endif; ?>
									<?php if ( get_sub_field('description_4') ): ?>
										<p><?php the_sub_field('description_4'); ?></p>
									<?php endif; ?>
									<?php $speaker_posts = get_sub_field('speaker_list_4'); ?>
									<?php if( $speaker_posts ): ?>
										<ul>
											<?php foreach( $speaker_posts as $post ): ?>
												<?php setup_postdata($post); ?>
												<li>
													<?php $image = get_field('headshot'); ?>
													<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($alt); ?>" />
													<div>
														<p class="name"><?php the_title(); ?></p>
														<p class="company"><?php the_field('company_name'); ?></p>
													</div>
												</li>
											<?php endforeach; ?>
										</ul>
										<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( get_field('track5_title_3') && get_sub_field('title_5') ): ?>
								<div class="description track5">
									<?php if ( get_sub_field('title_5') ): ?>
										<h3>
											<?php if ( get_sub_field('hide_track_05_title') ): ?><?php else: ?><span><?php the_field('track5_title_3'); ?></span><?php endif; ?>
											<?php the_sub_field('title_5'); ?>
										</h3>
									<?php endif; ?>
									<?php if ( get_sub_field('description_5') ): ?>
										<p><?php the_sub_field('description_5'); ?></p>
									<?php endif; ?>
									<?php $speaker_posts = get_sub_field('speaker_list_5'); ?>
									<?php if( $speaker_posts ): ?>
										<ul>
											<?php foreach( $speaker_posts as $post ): ?>
												<?php setup_postdata($post); ?>
												<li>
													<?php $image = get_field('headshot'); ?>
													<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($alt); ?>" />
													<div>
														<p class="name"><?php the_title(); ?></p>
														<p class="company"><?php the_field('company_name'); ?></p>
													</div>
												</li>
											<?php endforeach; ?>
										</ul>
										<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>


		<?php if( have_rows('article') ):  ?>
			<?php get_template_part('template-parts/article'); ?>
		<?php endif; ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>