<?php 
/*----------------------------------------------------------------*\

	Template Name: Confirmation

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/cookie-bar'); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main id="main-content">
	<header class="is-narrow">
		<h1>
			<?php if ( get_field('preheader') ) : ?>
				<span><?php the_field('preheader'); ?></span>
			<?php endif; ?>
			<?php the_title(); ?>
		</h1>
	</header>
</main>
<?php if ( get_field('button_primary') ) : ?>
	<aside class="helpful-links is-narrow">
		<p>These pages might be helpful as well:</p>
		<?php 
			$link = get_field('button_primary');
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
		?>
		<a class="button is-pink" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
		<?php if( get_field('button_secondary') ): ?>
				<?php 
					$link = get_field('button_secondary');
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="button is-green" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
			<?php endif; ?>
			<?php if( get_field('button_tertiary') ): ?>
				<?php 
					$link = get_field('button_tertiary');
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="button is-ghost" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
			<?php endif; ?>
	</aside>
<?php endif; ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>