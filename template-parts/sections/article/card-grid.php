<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying grid of cards

\*----------------------------------------------------------------*/
?>
<section id="section-<?php echo $template_args['sectionId']; ?>" class="card-grid <?php the_sub_field('format'); ?>-cards is-wide columns-2">
	<?php while ( have_rows('cards') ) : the_row(); ?>
		<div class="card">
			<?php if ( get_sub_field('image') ) : ?>
				<?php $image = get_sub_field('image'); ?>
				<figure>
					<img class="lazyload blur-up" data-expand="75" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
				</figure>
			<?php endif; ?>
			<?php if ( get_sub_field('title') ) : ?>
				<h3 class="is-<?php the_sub_field('gradient'); ?>">
					<?php the_sub_field('title') ?>
				</h3>
			<?php endif; ?>
			<div>
				<?php if ( get_sub_field('description') ) : ?>
					<p><?php the_sub_field('description'); ?></p>
				<?php endif; ?>
				<?php if ( get_sub_field('speaker_headshots') ) : ?>
					<?php $featured_posts = get_sub_field('speaker_headshots'); ?>
					<div class="speakers">
						<?php foreach( $featured_posts as $featured_post ): ?>
							<?php $headshot = get_field('headshot', $featured_post->ID); ?>
							<?php $logo = get_field('company_logo', $featured_post->ID); ?>
							<div class="speaker">
								<img class="headshot lazyload" data-expand="250" data-sizes="auto" src="<?php echo $headshot['sizes']['small']; ?>" data-src="<?php echo $headshot['sizes']['large']; ?>" data-srcset="<?php echo $headshot['sizes']['small']; ?> 350w, <?php echo $headshot['sizes']['medium']; ?> 600w, <?php echo $headshot['sizes']['large']; ?> 1000w, <?php echo $headshot['sizes']['xlarge']; ?> 1500w"  alt="<?php echo $headshot['alt']; ?>">
								<div class="logo">
									<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $logo['sizes']['small']; ?>" data-src="<?php echo $logo['sizes']['large']; ?>" data-srcset="<?php echo $logo['sizes']['small']; ?> 350w, <?php echo $logo['sizes']['medium']; ?> 600w, <?php echo $logo['sizes']['large']; ?> 1000w, <?php echo $logo['sizes']['xlarge']; ?> 1500w"  alt="<?php echo $logo['alt']; ?>">
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
				<?php if ( get_sub_field('primary_button') ) : ?>
					<div class="actions">
						<?php 
							$link = get_sub_field('primary_button'); 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self'; 
						?>
						<a class="button is-pink" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
							<?php echo esc_html($link_title); ?>
						</a>
						<?php if ( get_sub_field('secondary_button') ) : ?>
							<?php 
								$link = get_sub_field('secondary_button'); 
								$link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self'; 
							?>
							<a class="button is-ghost" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
								<?php echo esc_html($link_title); ?>
							</a>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endwhile; ?>
</section>