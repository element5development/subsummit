<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying grid of ticket cards

\*----------------------------------------------------------------*/
?>
<section id="section-<?php echo $template_args['sectionId']; ?>" class="ticket-cards is-standard">
	<?php while ( have_rows('cards') ) : the_row(); ?>
		<div class="ticket-card <?php if ( get_sub_field('virtual') ) : ?> is-virtual<?php endif; ?>">
			<?php if ( get_sub_field('title') ) : ?>
				<h2><?php the_sub_field('title'); ?></h2>
			<?php endif; ?>
			<?php if ( get_sub_field('availability') ) : ?>
				<p><?php the_sub_field('availability'); ?></p>
			<?php endif; ?>
			<div class="card-wrapper">
				<div class="top is-<?php the_sub_field('background'); ?>">
					<?php if ( get_sub_field('top_title') ) : ?>
						<p><?php the_sub_field('top_title'); ?></p>
					<?php endif; ?>
					<?php if ( get_sub_field('price') ) : ?>
						<h2><?php the_sub_field('price'); ?></h2>
					<?php endif; ?>
					<?php if ( get_sub_field('savings') ) : ?>
						<p><?php the_sub_field('savings'); ?></p>
					<?php endif; ?>
					<?php if ( get_sub_field('primary_button') ) : ?>
						<?php 
							$link = get_sub_field('primary_button'); 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self'; 
						?>
						<a class="button is-green" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
							<?php echo esc_html($link_title); ?>
						</a>
					<?php endif; ?>
					<?php if ( get_sub_field('register') ) : ?>
						<?php 
							$link = get_sub_field('register'); 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self'; 
						?>
						<a class="button is-ghost" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
							<?php echo esc_html($link_title); ?>
						</a>
					<?php endif; ?>
				</div>
				<div class="bottom">
					<?php if ( get_sub_field('bottom_title') ) : ?>
						<p><?php the_sub_field('bottom_title'); ?></p>
					<?php endif; ?>
					<?php if ( get_sub_field('bottom_price') ) : ?>
						<h2><?php the_sub_field('bottom_price'); ?></h2>
					<?php endif; ?>
					<?php if ( get_sub_field('bottom_savings') ) : ?>
						<p><?php the_sub_field('bottom_savings'); ?></p>
					<?php endif; ?>
					<?php if ( get_sub_field('bottom_button') ) : ?>
						<?php 
							$link = get_sub_field('bottom_button'); 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self'; 
						?>
						<a class="button is-ghost" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
							<?php echo esc_html($link_title); ?>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<?php endwhile; ?>
</section>