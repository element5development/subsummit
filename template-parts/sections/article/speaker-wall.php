<section class="speaker-grid is-extra-wide">
	<?php if ( get_sub_field('description') ) : ?>
		<div class="intro">
			<?php the_sub_field('description'); ?>
		</div>
	<?php endif; ?>
	<?php $featured_posts = get_sub_field('speakers'); ?>
	<?php foreach( $featured_posts as $featured_post ): ?>
		<article class="single-speaker">
			<?php $headshot = get_field('headshot', $featured_post->ID); ?>
			<div class="headshot">
				<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $headshot['sizes']['placeholder']; ?>" data-src="<?php echo $headshot['sizes']['small']; ?>" alt="<?php echo $headshot['alt']; ?>" />
			</div>
			<?php $logo = get_field('company_logo', $featured_post->ID); ?>
			<div class="company-logo">
				<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $logo['sizes']['largeplaceholder']; ?>" data-src="<?php echo $logo['sizes']['small']; ?>" alt="<?php echo $logo['alt']; ?>" />
			</div>
			<h4><?php echo get_the_title( $featured_post->ID ); ?></h4>
			<p><small><?php the_field('title', $featured_post->ID); ?></small></p>
		</article>
	<?php endforeach; ?>
</section>