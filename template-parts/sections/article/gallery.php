<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying gallery of images

\*----------------------------------------------------------------*/
?>

<?php //GALLERY
	$images = get_sub_field('gallery');
	$columns = get_sub_field('columns');
?>


<?php if ( get_sub_field('slider') == 'slider' ) : ?>
	<section id="section-<?php echo $template_args['sectionId']; ?>" class="gallery slider is-extra-wide">
		<ul>
			<?php foreach( $images as $image ): ?>
				<li>
					<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['small']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 600w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1500w"  alt="<?php echo $image['alt']; ?>">
				</li>
			<?php endforeach; ?>
		</ul>
	</section>
<?php else : ?>
	<section id="section-<?php echo $template_args['sectionId']; ?>" class="gallery <?php the_sub_field('width'); ?> columns-<?php echo $columns; ?>">
		<?php foreach( $images as $image ): ?>
			<figure>
				<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
			</figure>
		<?php endforeach; ?>
	</section>
<?php endif; ?>
