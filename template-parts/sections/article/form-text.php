<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying 2 columns one with a image the other with an editor

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="form-text">
	<div class="is-wide <?php the_sub_field('form_alignment'); ?>">
		<div>
			<?php the_sub_field('content'); ?>
		</div>
		<div>
			<?php the_sub_field('form'); ?>
		</div>
	</div>
</section>