<section id="section-<?php echo $template_args['sectionId']; ?>" class="sponsor-slider is-extra-wide">
	<div class="sponsors">
		<?php $featured_posts = get_sub_field('sponsors'); ?>
		<?php foreach( $featured_posts as $featured_post ): ?>
			<div class="sponsor">
				<?php $logo = get_field('logo', $featured_post->ID ); ?>
				<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $logo['sizes']['small']; ?>" data-src="<?php echo $logo['sizes']['large']; ?>" data-srcset="<?php echo $logo['sizes']['small']; ?> 350w, <?php echo $logo['sizes']['medium']; ?> 600w, <?php echo $logo['sizes']['large']; ?> 1000w, <?php echo $logo['sizes']['xlarge']; ?> 1500w"  alt="<?php echo $logo['alt']; ?>">
			</div>
		<?php endforeach; ?>
	</div>
</section>