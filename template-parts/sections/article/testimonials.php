<section id="section-<?php echo $template_args['sectionId']; ?>" class="testimonial-slider is-extra-wide">
	<div class="testimonials">
		<?php while( have_rows('testimonials') ) : the_row(); ?>
			<div class="testimonial">
				<?php $logo = get_sub_field('logo'); ?>
				<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $logo['sizes']['small']; ?>" data-src="<?php echo $logo['sizes']['large']; ?>" data-srcset="<?php echo $logo['sizes']['small']; ?> 350w, <?php echo $logo['sizes']['medium']; ?> 600w, <?php echo $logo['sizes']['large']; ?> 1000w, <?php echo $logo['sizes']['xlarge']; ?> 1500w"  alt="<?php echo $logo['alt']; ?>">
				<blockquote>
					<?php the_sub_field('quote'); ?>
					<p><?php the_sub_field('quotee'); ?></p>
				</blockquote>
			</div>
		<?php endwhile; ?>
	</div>
</section>