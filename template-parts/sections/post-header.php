<?php 
/*----------------------------------------------------------------*\

	POST HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>

<header class="post-head <?php if ( get_field('header_description') || get_field('header_button_primary') || get_field('header_button_secondary') ): ?>is-tall<?php endif; ?>">
	<div class="is-standard">
		<h1>
			<?php if ( get_field('preheader') ) : ?>
				<span><?php the_field('preheader'); ?></span>
			<?php endif; ?>
			<?php the_title(); ?>
		</h1>
		<?php if ( get_field('presented_by') ) : ?>
			<?php if ( is_page('3575') ) : ?>
				<p class="presented-by">Live stream sponsored by</p>
			<?php else : ?>
				<p class="presented-by">Presented By</p>
			<?php endif; ?>
			<?php $logo = get_field('presented_by'); ?>
			<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['small']; ?>" alt="<?php echo $logo['alt']; ?>" />
		<?php endif; ?>
		<?php if ( get_field('header_description') ) : ?>
			<?php the_field('header_description'); ?>
		<?php endif; ?>
		<?php if( get_field('header_button_primary') ): ?>
			<?php 
				$link = get_field('header_button_primary');
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
			?>
			<a class="button is-pink" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
		<?php endif; ?>
		<?php if( get_field('header_button_secondary') ): ?>
			<?php 
				$link = get_field('header_button_secondary');
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
			?>
			<a class="button is-ghost" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
		<?php endif; ?>
	</div>
</header>