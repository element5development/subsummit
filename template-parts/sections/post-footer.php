<?php 
/*----------------------------------------------------------------*\

	POST FOOTER
	Display copyright and navigation

\*----------------------------------------------------------------*/
?>
<aside class="subta-powered">
	<div class="is-standard">
		<svg>
			<use xlink:href="#subsummit-white"></use>
		</svg>
		<div>
			<h2>Join Subta and Receive the Largest Discount</h2>
			<p>SUBTA is an ever-growing community of innovators, entrepreneurs, thought leaders and dedicated teams that are eager to scale their businesses and catalyze the subscription industry. Sound like you?</p>
			<a target="_blank" href="https://subta.com/" class="button is-pink">Learn About SUBTA Memberships</a>
		</div>
	</div>
</aside>
<footer class="post-footer">
	<div class="is-extra-wide">
		<div class="column column-one">
			<ul>
				<li><small>info@subta.com</small></li>
				<li><small>+1 833-637-8282</small></li>
				<li><small>901 Wilshire Dr., Suite 190<br/>Troy, MI 48084</small></li>
			</ul>
		</div>
		<div class="column column-two">
			<?php dynamic_sidebar( 'footer-one' ); ?>
		</div>
		<div class="column column-three">
			<?php dynamic_sidebar( 'footer-two' ); ?>
		</div>
		<div class="column column-four">
			<?php dynamic_sidebar( 'footer-three' ); ?>
		</div>
		<div class="column column-five">
			<?php dynamic_sidebar( 'footer-four' ); ?>
		</div>
		<div class="column column-six">
			<nav class="social">
				<a target="_blank" href="https://twitter.com/Sub_Summit">
					<svg viewBox="0 0 32 32">
						<use xlink:href="#twitter"></use>
					</svg>
				</a>
				<a target="_blank" href="https://www.instagram.com/sub_summit/">
					<svg viewBox="0 0 32 32">
						<use xlink:href="#instagram"></use>
					</svg>
				</a>
				<a target="_blank" href="https://www.facebook.com/subscriptionsummit/">
					<svg viewBox="0 0 32 32">
						<use xlink:href="#facebook"></use>
					</svg>
				</a>
				<a target="_blank" href="https://www.linkedin.com/company/subsummit/">
					<svg viewBox="0 0 32 32">
						<use xlink:href="#linkedin"></use>
					</svg>
				</a>
				<a target="_blank" href="https://www.youtube.com/channel/UCSSrvzhyrt01g3Adx4z98BQ">
					<svg viewBox="0 0 32 32">
						<use xlink:href="#youtube"></use>
					</svg>
				</a>
			</nav>
		</div>
		<div class="copyright">
			<nav><?php wp_nav_menu(array( 'theme_location' => 'legal_navigation' )); ?></nav>
			<div>
				<p><small>©Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.</small></p>
				<a target="_blank" href="https://element5digital.com"> 
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/element5.svg" alt="Crafted by Element5 Digital"> 
				</a>
			</div>
		</div>
	</div>
</footer>