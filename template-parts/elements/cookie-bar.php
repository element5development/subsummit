<?php 
/*----------------------------------------------------------------*\

	COOKIE USE NOTIFICATION BAR
	cookies used must be cleared via WPengine support

\*----------------------------------------------------------------*/
?>
<div class="cookie-useage-notification note-on <?php if ( get_field('price_jump_date', 'options') ) : ?>is-higher-up<?php endif; ?>">
	<div> 
		<button> 
			<svg><use xlink:href="#close"></use></svg> 
		</button>
		<p><b>This site uses cookies</b></p>
		<p><small>This allows us to provide you with a greater user experience. By using our website, you accept our <a href="<?php echo get_privacy_policy_url(); ?>">use of cookies</a>.</small></p>
	</div>
</div>