<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>
<div class="primary-nav-wrap">
	<?php if ( get_field('notification', 'options') ) : ?>
		<aside class="notification">	
			<p><?php the_field('notification', 'options'); ?></p>
		</aside>
	<?php endif; ?>
	<nav class="is-primary is-horizontal is-extra-wide">
		<div class="left">
			<a class="subsum-logo" href="<?php echo get_home_url(); ?>">
				<svg>
					<use xlink:href="#subsummit"></use>
				</svg>
			</a>
			<?php wp_nav_menu(array( 'theme_location' => 'primary_left_navigation' )); ?>
		</div>
		<div class="right">
			<?php wp_nav_menu(array( 'theme_location' => 'primary_right_navigation' )); ?>
			<button class="mobile-menu-toggle is-pink">
				Menu
			</button>
		</div>
	</nav>
</div>

<nav class="mobile-menu">
	<button class="mobile-menu-toggle is-ghost">
		Close
	</button>
	<?php wp_nav_menu(array( 'theme_location' => 'full_navigation' )); ?>
</nav>

<?php if ( get_field('price_jump_date', 'options') ) : ?>
	<aside class="price-jump">
		<div class="container is-standard">
			<p><span class="label">Price Increases in</span> <span class="counter days">00</span> Days <span class="counter hours">00</span> Hours <span class="counter minutes">00</span> Minutes <span class="counter seconds">00</span> Seconds</p>
			<a href="" class="button is-green">Purchase Tickets</a>
		</div>
	</aside>
<?php endif; ?>