<?php 
/*----------------------------------------------------------------*\

	DEFAULT CATEGORY ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php //platinum sponsors loop
	$args_platinum_sponsors = array(
		'post_type' => array('sponsor'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'order' => 'DESC',
		'meta_query' => array(
        array(
            'key' => 'level',
            'value' => 'platinum',
        )
    )
	);
	$platinum_sponsors = new WP_Query( $args_platinum_sponsors );
?>
<?php //gold sponsors loop
	$args_gold_sponsors = array(
		'post_type' => array('sponsor'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'order' => 'DESC',
		'meta_query' => array(
        array(
            'key' => 'level',
            'value' => 'gold',
        )
    )
	);
	$gold_sponsors = new WP_Query( $args_gold_sponsors );
?>
<?php //silver sponsors loop
	$args_silver_sponsors = array(
		'post_type' => array('sponsor'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'order' => 'DESC',
		'meta_query' => array(
        array(
            'key' => 'level',
            'value' => 'silver',
        )
    )
	);
	$silver_sponsors = new WP_Query( $args_silver_sponsors );
?>
<?php //bronze sponsors loop
	$args_bronze_sponsors = array(
		'post_type' => array('sponsor'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'order' => 'DESC',
		'meta_query' => array(
        array(
            'key' => 'level',
            'value' => 'bronze',
        )
    )
	);
	$bronze_sponsors = new WP_Query( $args_bronze_sponsors );
?>
<?php //other sponsors loop
	$args_other_sponsors = array(
		'post_type' => array('sponsor'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'order' => 'DESC',
		'meta_query' => array(
        array(
            'key' => 'level',
            'value' => 'other',
        )
    )
	);
	$other_sponsors = new WP_Query( $args_other_sponsors );
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head <?php echo $class; ?>">
	<div class="is-standard">
		<h1>
			<?php if ( get_field('sponsor_preheader','option') ) : ?>
				<span><?php the_field('sponsor_preheader','option'); ?></span>
			<?php endif; ?>
			<?php the_field('sponsor_title','option'); ?>
		</h1>
		<?php if( get_field('sponsor_button','option') ): ?>
			<?php 
				$link = get_field('sponsor_button','option');
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
			?>
			<a class="button is-pink" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
		<?php endif; ?>
	</div>
</header>

<main id="main-content">	
	<article>
		<?php if ( get_field('sponsor_left_column', 'option') && get_field('sponsor_right_column', 'option') ) : ?>
			<section class="editor-2-column is-standard is-fifty">
				<div>
					<?php the_field('sponsor_left_column', 'option'); ?>
				</div>
				<div>
					<?php the_field('sponsor_right_column', 'option'); ?>
				</div>
			</section>
		<?php endif; ?>

		<?php if ( $platinum_sponsors->have_posts() ) : ?>
			<section class="sponsors is-wide">
				<h2>Platinum<br/>Sponsors</h2>
				<div class="sponsor-grid platinum">
					<?php while ( $platinum_sponsors->have_posts() ) : $platinum_sponsors->the_post(); ?>
						<div class="sponsor">
							<a href="<?php the_permalink(); ?>">
								<?php $logo = get_field('logo'); ?>
								<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['large']; ?>" data-srcset="<?php echo $logo['sizes']['small']; ?> 300w, <?php echo $logo['sizes']['medium']; ?> 700w, <?php echo $logo['sizes']['large']; ?> 1000w, <?php echo $logo['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $logo['alt']; ?>">
							</a>
						</div>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; wp_reset_postdata(); ?>
		<?php if ( $gold_sponsors->have_posts() ) : ?>
			<section class="sponsors is-wide">
				<h2>Gold<br/>Sponsors</h2>
				<div class="sponsor-grid gold">
					<?php while ( $gold_sponsors->have_posts() ) : $gold_sponsors->the_post(); ?>
						<div class="sponsor">
							<a href="<?php the_permalink(); ?>">
								<?php $logo = get_field('logo'); ?>
								<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['large']; ?>" data-srcset="<?php echo $logo['sizes']['small']; ?> 300w, <?php echo $logo['sizes']['medium']; ?> 700w, <?php echo $logo['sizes']['large']; ?> 1000w, <?php echo $logo['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $logo['alt']; ?>">
							</a>
						</div>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; wp_reset_postdata(); ?>
		<?php if ( $silver_sponsors->have_posts() ) : ?>
			<section class="sponsors is-wide">
				<h2>Silver<br/>Sponsors</h2>
				<div class="sponsor-grid silver">
					<?php while ( $silver_sponsors->have_posts() ) : $silver_sponsors->the_post(); ?>
						<div class="sponsor">
							<a href="<?php the_permalink(); ?>">
								<?php $logo = get_field('logo'); ?>
								<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['large']; ?>" data-srcset="<?php echo $logo['sizes']['small']; ?> 300w, <?php echo $logo['sizes']['medium']; ?> 700w, <?php echo $logo['sizes']['large']; ?> 1000w, <?php echo $logo['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $logo['alt']; ?>">
							</a>
						</div>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; wp_reset_postdata(); ?>
		<?php if ( $bronze_sponsors->have_posts() ) : ?>
			<section class="sponsors is-wide">
				<h2>Bronze<br/>Sponsors</h2>
				<div class="sponsor-grid bronze">
					<?php while ( $bronze_sponsors->have_posts() ) : $bronze_sponsors->the_post(); ?>
						<div class="sponsor">
							<a href="<?php the_permalink(); ?>">
								<?php $logo = get_field('logo'); ?>
								<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['large']; ?>" data-srcset="<?php echo $logo['sizes']['small']; ?> 300w, <?php echo $logo['sizes']['medium']; ?> 700w, <?php echo $logo['sizes']['large']; ?> 1000w, <?php echo $logo['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $logo['alt']; ?>">
							</a>
						</div>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; wp_reset_postdata(); ?>
		<?php if ( $other_sponsors->have_posts() ) : ?>
			<section class="sponsors other is-wide">
				<h2>Additional<br/>Sponsors</h2>
				<div class="sponsor-grid other">
					<?php while ( $other_sponsors->have_posts() ) : $other_sponsors->the_post(); ?>
						<div class="sponsor">
							<a href="<?php the_permalink(); ?>">
								<?php $logo = get_field('logo'); ?>
								<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['large']; ?>" data-srcset="<?php echo $logo['sizes']['small']; ?> 300w, <?php echo $logo['sizes']['medium']; ?> 700w, <?php echo $logo['sizes']['large']; ?> 1000w, <?php echo $logo['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $logo['alt']; ?>">
							</a>	
						</div>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; wp_reset_postdata(); ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>