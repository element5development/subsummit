<?php 
/*----------------------------------------------------------------*\

	DEFAULT PAGE TEMPLATE

\*----------------------------------------------------------------*/
?>


<?php //SKIP REPEAT LOGIN FOR 12 HOURS
	if ( is_page(3575) && isset($_COOKIE['live_login']) ) : 
		header('location: https://subsummit.com/watch-now/');
		exit;
	endif;
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/cookie-bar'); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php 
	$hideHeader = 0;
	if( have_rows('article') ) : 
		$rows = get_field('article');
		$i = 0;
		while ( have_rows('article') ) : the_row();
			$i++;
			if( $i == 1 && get_row_layout() == 'price_cards' ) :
				$hideHeader = 1;
			endif;
			$totalSections = $i;
		endwhile;
	endif;
?>

<?php if ( $hideHeader == 0 ) : ?>
	<?php get_template_part('template-parts/sections/post-header'); ?>
<?php else : ?>
	<header class="post-head is-empty">
	</header>
<?php endif; ?>

<main id="main-content" class="<?php if ( $totalSections <= 2 ) : ?>is-short<?php endif; ?>">
	<?php if( have_rows('article') ):  ?>
		<article>
			<?php get_template_part('template-parts/article'); ?>
		</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>