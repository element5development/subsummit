<?php
/*----------------------------------------------------------------*\
	INITIALIZE MENUS
\*----------------------------------------------------------------*/
function nav_creation() {
	$locations = array(
		'primary_left_navigation' => __( 'Primary Left Menu' ),
		'primary_right_navigation' => __( 'Primary Right Menu' ),
		'legal_navigation' => __( 'Legal Menu' ),
		'full_navigation' => __( 'Full Menu' ),
	);
	register_nav_menus( $locations );
}
add_action( 'init', 'nav_creation' );