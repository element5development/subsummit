<?php
/*----------------------------------------------------------------*\
	REMOVE ADMIN BAR NODES
\*----------------------------------------------------------------*/
add_filter('autoptimize_filter_toolbar_show','__return_false');

/*----------------------------------------------------------------*\
	REMOVE UNNEEDED DASHBOARD WIDGETS
\*----------------------------------------------------------------*/
function remove_dashboard_widgets() {
	global $wp_meta_boxes;
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );

/*----------------------------------------------------------------*\
	DEVELOPER DASHBOARD WIDGET
\*----------------------------------------------------------------*/
function my_custom_dashboard_widgets() {
	global $wp_meta_boxes;
	wp_add_dashboard_widget('custom_help_widget', 'Need Help? Have Questions?', 'custom_dashboard_help');
}
function custom_dashboard_help() {
	echo '<p>When the time comes you need to update your site or have some question on how to use WordPress or just confused feel free to contact us.<br><a href="mailto:support@element5digital.com">support@element5digital.com</a><br><a href="tel:+12485301000">(248) 530-1000</a><br><a target="_blank" href="https://element5digital.com/">element5digital.com</a><img src="/wp-content/themes/subsummit/dist/images/element5-logo.svg" alt="Element5 Digital" />';
}
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');

/*----------------------------------------------------------------*\
	CUSTOM CSS FOR WP ADMIN AREA
\*----------------------------------------------------------------*/
// function my_custom_admin() {
//   echo '
// 		<style>
// 		</style>
// 	';
// }
// add_action('admin_head', 'my_custom_admin');