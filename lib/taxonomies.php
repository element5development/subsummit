<?php
/*----------------------------------------------------------------*\
	INITIALIZE TAXONOMIES
\*----------------------------------------------------------------*/
// Register Taxonomy Room
function create_room_tax() {
	$labels = array(
		'name'              => _x( 'Rooms', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Room', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Rooms', 'textdomain' ),
		'all_items'         => __( 'All Rooms', 'textdomain' ),
		'parent_item'       => __( 'Parent Room', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Room:', 'textdomain' ),
		'edit_item'         => __( 'Edit Room', 'textdomain' ),
		'update_item'       => __( 'Update Room', 'textdomain' ),
		'add_new_item'      => __( 'Add New Room', 'textdomain' ),
		'new_item_name'     => __( 'New Room Name', 'textdomain' ),
		'menu_name'         => __( 'Room', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => false,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
		'show_in_rest' => true,
	);
	register_taxonomy( 'room', array('live'), $args );
}
add_action( 'init', 'create_room_tax' );