<?php
/*----------------------------------------------------------------*\
	ADDITIONAL IMAGE SIZES
\*----------------------------------------------------------------*/
add_image_size( 'placeholder', 20, 20 ); 
function placeholder_image_sizes($sizes) {
	$sizes['placeholder'] = __( 'Placeholder' );
	return $sizes;
}
add_filter('image_size_names_choose', 'placeholder_image_sizes');
add_image_size( 'largeplaceholder', 100, 100 ); 
function largePlaceholder_image_sizes($sizes) {
	$sizes['largeplaceholder'] = __( 'largePlaceholder' );
	return $sizes;
}
add_filter('image_size_names_choose', 'largePlaceholder_image_sizes');
add_image_size( 'xsmall', 200, 200 ); 
function xsmall_image_sizes($sizes) {
	$sizes['xsmall'] = __( 'xSmall' );
	return $sizes;
}
add_filter('image_size_names_choose', 'xsmall_image_sizes');
add_image_size( 'small', 400, 400 ); 
function small_image_sizes($sizes) {
	$sizes['small'] = __( 'Small' );
	return $sizes;
}
add_filter('image_size_names_choose', 'small_image_sizes');
add_image_size( 'xlarge', 2000, 2000 ); 
function large_image_sizes($sizes) {
	$sizes['xlarge'] = __( 'X-Large' );
	return $sizes;
}
add_filter('image_size_names_choose', 'large_image_sizes');

/*----------------------------------------------------------------*\
    ADDITIONAL FILE FORMATS ~ svg 2020 fix
\*----------------------------------------------------------------*/
add_filter( 'wp_check_filetype_and_ext', function($data, $file, $filename, $mimes) {
  global $wp_version;
  if ( $wp_version !== '4.7.1' ) {
     return $data;
  }
  $filetype = wp_check_filetype( $filename, $mimes );
  return [
      'ext'             => $filetype['ext'],
      'type'            => $filetype['type'],
      'proper_filename' => $data['proper_filename']
  ];
}, 10, 4 );

function cc_mime_types( $mimes ){
	$mimes['svg'] = 'image/svg+xml';
	$mimes['zip'] = 'application/zip';
  return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

function fix_svg() {
  echo '<style type="text/css">
        .attachment-266x266, .thumbnail img {
             width: 100% !important;
             height: auto !important;
        }
        </style>';
}
add_action( 'admin_head', 'fix_svg' );

/*----------------------------------------------------------------*\
	LAZY LOAD CLASSES FOR WYSIWYG
\*----------------------------------------------------------------*/
add_filter('acf_the_content', 'filter');
function filter($content) {
  return str_replace('src="', 'data-expand="250" data-src="', $content);
}

function add_image_class($class){
	$class .= ' lazyload blur-up';
	return $class;
}
add_filter('get_image_tag_class','add_image_class');