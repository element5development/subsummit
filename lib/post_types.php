<?php
/*----------------------------------------------------------------*\
	INITIALIZE POST TYPES
\*----------------------------------------------------------------*/
// Register Custom Post Type Speaker
function create_speaker_cpt() {
	$labels = array(
		'name' => _x( 'Speakers', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Speaker', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Speakers', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Speaker', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Speaker Archives', 'textdomain' ),
		'attributes' => __( 'Speaker Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Speaker:', 'textdomain' ),
		'all_items' => __( 'All Speakers', 'textdomain' ),
		'add_new_item' => __( 'Add New Speaker', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Speaker', 'textdomain' ),
		'edit_item' => __( 'Edit Speaker', 'textdomain' ),
		'update_item' => __( 'Update Speaker', 'textdomain' ),
		'view_item' => __( 'View Speaker', 'textdomain' ),
		'view_items' => __( 'View Speakers', 'textdomain' ),
		'search_items' => __( 'Search Speaker', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Speaker', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Speaker', 'textdomain' ),
		'items_list' => __( 'Speakers list', 'textdomain' ),
		'items_list_navigation' => __( 'Speakers list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Speakers list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Speaker', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-groups',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'page-attributes', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'speaker', $args );
}
add_action( 'init', 'create_speaker_cpt', 0 );

function change_posts_per_page( $query ) {
	if ( is_post_type_archive( 'speaker' ) ) {
		 $query->set( 'posts_per_page', -1 );
	}
}
add_filter( 'pre_get_posts', 'change_posts_per_page' );

// Register Custom Post Type Sponsor
function create_sponsor_cpt() {
	$labels = array(
		'name' => _x( 'Sponsors', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Sponsor', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Sponsors', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Sponsor', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Sponsor Archives', 'textdomain' ),
		'attributes' => __( 'Sponsor Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Sponsor:', 'textdomain' ),
		'all_items' => __( 'All Sponsors', 'textdomain' ),
		'add_new_item' => __( 'Add New Sponsor', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Sponsor', 'textdomain' ),
		'edit_item' => __( 'Edit Sponsor', 'textdomain' ),
		'update_item' => __( 'Update Sponsor', 'textdomain' ),
		'view_item' => __( 'View Sponsor', 'textdomain' ),
		'view_items' => __( 'View Sponsors', 'textdomain' ),
		'search_items' => __( 'Search Sponsor', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Sponsor', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Sponsor', 'textdomain' ),
		'items_list' => __( 'Sponsors list', 'textdomain' ),
		'items_list_navigation' => __( 'Sponsors list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Sponsors list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Sponsor', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-awards',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'page-attributes', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'sponsor', $args );
}
add_action( 'init', 'create_sponsor_cpt', 0 );

// Register Custom Post Type Live
function create_live_cpt() {
	$labels = array(
		'name' => _x( 'Live', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Live', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Live', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Live', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Live Archives', 'textdomain' ),
		'attributes' => __( 'Live Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Live:', 'textdomain' ),
		'all_items' => __( 'All Live', 'textdomain' ),
		'add_new_item' => __( 'Add New Live', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Live', 'textdomain' ),
		'edit_item' => __( 'Edit Live', 'textdomain' ),
		'update_item' => __( 'Update Live', 'textdomain' ),
		'view_item' => __( 'View Live', 'textdomain' ),
		'view_items' => __( 'View Live', 'textdomain' ),
		'search_items' => __( 'Search Live', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Live', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Live', 'textdomain' ),
		'items_list' => __( 'Live list', 'textdomain' ),
		'items_list_navigation' => __( 'Live list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Live list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Live', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-video-alt3',
		'supports' => array('title', 'editor', 'custom-fields'),
		'taxonomies' => array('room'),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => true,
		'show_in_rest' => true,
		'rewrite' => array(
			'slug' => 'watch-now'
		),
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'live', $args );
}
add_action( 'init', 'create_live_cpt', 0 );