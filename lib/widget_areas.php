<?php
/*----------------------------------------------------------------*\
	INITIALIZE WIDGET AREA
\*----------------------------------------------------------------*/
function footer_areas() {

	$args = array(
		'name'          => __( 'Footer Column One', 'textdomain' ),
		'id'            => 'footer-one',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5 class="widgettitle">',
		'after_title'   => '</h5>',
	);
	register_sidebar($args);

	$args = array(
		'name'          => __( 'Footer Column Two', 'textdomain' ),
		'id'            => 'footer-two',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5 class="widgettitle">',
		'after_title'   => '</h5>',
	);
	register_sidebar($args);

	$args = array(
		'name'          => __( 'Footer Column Three', 'textdomain' ),
		'id'            => 'footer-three',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5 class="widgettitle">',
		'after_title'   => '</h5>',
	);
	register_sidebar($args);

	$args = array(
		'name'          => __( 'Footer Column Four', 'textdomain' ),
		'id'            => 'footer-four',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5 class="widgettitle">',
		'after_title'   => '</h5>',
	);
	register_sidebar($args);

}
add_action( 'widgets_init', 'footer_areas' );