<?php
/*----------------------------------------------------------------*\
	OPTION PAGES
\*----------------------------------------------------------------*/
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Archives',
		'menu_title'	=> 'Archives',
		'menu_slug' 	=> 'archives-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	acf_add_options_page(array(
		'page_title' 	=> 'Notification',
		'menu_title'	=> 'Notification',
		'menu_slug' 	=> 'notification-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}
?>