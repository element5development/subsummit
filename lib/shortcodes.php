<?php
/*----------------------------------------------------------------*\
	INITIALIZE BUTTON SHORTCODE
\*----------------------------------------------------------------*/
function button_shortcode($atts, $content = null) {
	$atts = shortcode_atts(
		array(
			'color' => 'orange',
			'target' => '_self',
			'url' => '#',
		),
		$atts,
		'button'
	);
	$size = $atts['size'];
	$color = $atts['color'];
	$target = $atts['target'];
	$url = $atts['url'];

	$link = '<a target="'.$target.'" href="'.$url.'" class="button is-'.$color.'">' . do_shortcode($content) . '</a>';
  return $link;
}
add_shortcode( 'button', 'button_shortcode' );
/*----------------------------------------------------------------*\
	INITIALIZE TICKET SHORTCODE
\*----------------------------------------------------------------*/
function merchant_ticket_shortcode($atts) {
	$atts = shortcode_atts(
		array(
			'price' => '$150',
		),
		$atts,
		'merchant-ticket'
	);
	$price = $atts['price'];
	return '
		<div class="price-cards is-wide columns-1">
			<div>
				<h2>Merchants</h2>
				<div class="type-grid">
					<div class="label spacer"></div>
					<div class="label type-one">
						<h3>Free</h3>
					</div>
					<div class="label type-two">
						<h3>Premium</h3>
						<p><strike>$300</strike> '.$price.'</p>
					</div>
					<div class="option label">SUBTA Theater Sessions</div>
						<div class="option type-one">
						<svg viewBox="0 0 26 28">
							<use xlink:href="#checkmark-filled"></use>
						</svg>
					</div>
					<div class="option type-two">
						<svg viewBox="0 0 26 28">
							<use xlink:href="#checkmark-filled"></use>
						</svg>
					</div>
					<div class="option label">Buyers Guide</div>
					<div class="option type-one">
						<svg viewBox="0 0 26 28">
							<use xlink:href="#checkmark-filled"></use>
						</svg>
					</div>
					<div class="option type-two">
						<svg viewBox="0 0 26 28">
							<use xlink:href="#checkmark-filled"></use>
						</svg>
					</div>
					<div class="option label">Access to Expo Hall</div>
					<div class="option type-one">
						<svg viewBox="0 0 26 28">
							<use xlink:href="#checkmark-filled"></use>
						</svg>
					</div>
					<div class="option type-two">
						<svg viewBox="0 0 26 28">
							<use xlink:href="#checkmark-filled"></use>
						</svg>
					</div>
					<div class="option label">Access to Lounges</div>
					<div class="option type-one">
						<svg viewBox="0 0 26 28">
							<use xlink:href="#checkmark-filled"></use>
						</svg>
					</div>
					<div class="option type-two">
						<svg viewBox="0 0 26 28">
							<use xlink:href="#checkmark-filled"></use>
						</svg>
					</div>
					<div class="option label">Access to Workshops</div>
					<div class="option type-one">
						<svg viewBox="0 0 26 28">
							<use xlink:href="#checkmark-filled"></use>
						</svg>
					</div>
					<div class="option type-two">
						<svg viewBox="0 0 26 28">
							<use xlink:href="#checkmark-filled"></use>
						</svg>
					</div>
					<div class="option label">Worlds Best Welcome Box</div>
					<div class="option type-one">
						<svg viewBox="0 0 26 28">
							<use xlink:href="#checkmark-empty"></use>
						</svg>
					</div>
					<div class="option type-two">
						<svg viewBox="0 0 26 28">
							<use xlink:href="#checkmark-filled"></use>
						</svg>
					</div>
					<div class="option label">VIP Networking Lounge Access</div>
					<div class="option type-one">
						<svg viewBox="0 0 26 28">
							<use xlink:href="#checkmark-empty"></use>
						</svg>
					</div>
					<div class="option type-two">
						<svg viewBox="0 0 26 28">
							<use xlink:href="#checkmark-filled"></use>
						</svg>
					</div>
					<div class="option label">After the Event Access to all Sessions</div>
					<div class="option type-one">
						<svg viewBox="0 0 26 28">
							<use xlink:href="#checkmark-empty"></use>
						</svg>
					</div>
					<div class="option type-two">
						<svg viewBox="0 0 26 28">
							<use xlink:href="#checkmark-filled"></use>
						</svg>
					</div>
					<div class="option label">After the Event Access to Expo Hall for 1-Week</div>
					<div class="option type-one">
						<svg viewBox="0 0 26 28">
							<use xlink:href="#checkmark-empty"></use>
						</svg>
					</div>
					<div class="option type-two">
						<svg viewBox="0 0 26 28">
							<use xlink:href="#checkmark-filled"></use>
						</svg>
					</div>
				</div>
			</div>
		</div>
	';
}
add_shortcode( 'merchant-ticket', 'merchant_ticket_shortcode' );
function supplier_ticket_shortcode($atts) {
	$atts = shortcode_atts(
		array(
		),
		$atts,
		'supplier-ticket'
	);
	return '
	<div class="price-cards is-wide columns-1">
		<div>
			<h2>Suppliers</h2>
			<div class="type-grid">
				<div class="label spacer"></div>
				<div class="label type-one">
					<h3>Free</h3>
				</div>
				<div class="label type-two">
					<h3>Premium</h3>
					<p>$500</p>
				</div>
				<div class="option label">SUBTA Theater Sessions</div>
				<div class="option type-one">
					<svg viewBox="0 0 26 28">
						<use xlink:href="#checkmark-filled"></use>
					</svg>
				</div>
				<div class="option type-two">
					<svg viewBox="0 0 26 28">
						<use xlink:href="#checkmark-filled"></use>
					</svg>
				</div>
				<div class="option label">Buyers Guide</div>
				<div class="option type-one">
					<svg viewBox="0 0 26 28">
						<use xlink:href="#checkmark-filled"></use>
					</svg>
				</div>
				<div class="option type-two">
					<svg viewBox="0 0 26 28">
						<use xlink:href="#checkmark-filled"></use>
					</svg>
				</div>
				<div class="option label">Access to Expo Hall</div>
				<div class="option type-one">
					<svg viewBox="0 0 26 28">
						<use xlink:href="#checkmark-filled"></use>
					</svg>
				</div>
				<div class="option type-two">
					<svg viewBox="0 0 26 28">
						<use xlink:href="#checkmark-filled"></use>
					</svg>
				</div>
				<div class="option label">Access to Lounges</div>
				<div class="option type-one">
					<svg viewBox="0 0 26 28">
						<use xlink:href="#checkmark-empty"></use>
					</svg>
				</div>
				<div class="option type-two">
					<svg viewBox="0 0 26 28">
						<use xlink:href="#checkmark-filled"></use>
					</svg>
				</div>
				<div class="option label">Worlds Best Welcome Box</div>
				<div class="option type-one">
					<svg viewBox="0 0 26 28">
						<use xlink:href="#checkmark-empty"></use>
					</svg>
				</div>
				<div class="option type-two">
					<svg viewBox="0 0 26 28">
						<use xlink:href="#checkmark-filled"></use>
					</svg>
				</div>
				<div class="option label">VIP Networking Lounge Access</div>
				<div class="option type-one">
					<svg viewBox="0 0 26 28">
						<use xlink:href="#checkmark-empty"></use>
					</svg>
				</div>
				<div class="option type-two">
					<svg viewBox="0 0 26 28">
						<use xlink:href="#checkmark-filled"></use>
					</svg>
				</div>
			</div>
		</div>
	</div>
	';
}
add_shortcode( 'supplier-ticket', 'supplier_ticket_shortcode' );
/*----------------------------------------------------------------*\
	INITIALIZE VIDEO SHORTCODE
\*----------------------------------------------------------------*/
function video_shortcode($atts, $content = null) {
	$atts = shortcode_atts(
		array(
			'id' => 'id',
		),
		$atts,
		'button'
	);
	$id = $atts['id'];

	$video = '
		<div class="video-wrapper">
			<iframe src="https://www.youtube.com/embed/'.$id.'" frameborder="0" allowfullscreen></iframe>
		</div>
	';
  return $video;
}
add_shortcode( 'video', 'video_shortcode' );