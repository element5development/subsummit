<?php 
/*----------------------------------------------------------------*\

	DEFAULT CATEGORY ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php // Find current date and time
	$universal_date_now = date('Y-m-d H:i:s');
	$universal_time_now = strtotime($universal_date_now);
	$detroit_time_now = strtotime('-5 hours', $universal_time_now);
	$detroit_date_now = date('Y-m-d H:i:s', $detroit_time_now);
	$detroit_date_now_value = date('YmdHis', $detroit_time_now);
	$future_time_now = strtotime('-4 hours', $universal_time_now);
	$future_date_now = date('Y-m-d H:i:s', $future_time_now);
	$future_date_now_value = date('YmdHis', $future_time_now);
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head is-wide">
	<?php //taxonomy
		$taxonomies = get_terms( array(
			'taxonomy' => 'room',
			'hide_empty' => true
		) );
		$page_id = $wp_query->get_queried_object_id();
	?>
	<nav>
		<ul>
			<li>
				<a href="/watch-now">All Rooms</a>
			</li>
			<?php foreach( $taxonomies as $category ) : ?>
				<?php
					if ( $page_id == $category->term_id ) :
						$class = "is-active";	
					else :
						$class = " ";
					endif;
				?>
				<li>
					<a href="<?php echo get_category_link( $category->term_id ); ?>" class="<?php echo $class; ?>"><?php echo $category->name ?></a>
				</li>
			<?php endforeach; ?>
		</ul>
	</nav>
</header>

<main id="main-content">
	<section class="room-grid is-wide">
		<?php //ROOM QUERY
			$args_room = array(
				'posts_per_page' => -1,
				'post_type'      => 'live',
				'tax_query' => array(
					array (
							'taxonomy' => 'room',
							'field' => 'id',
							'terms' => $page_id,
					)
				),
				'meta_query'     => array(
					array(
							'key'         => 'end_time',
							'compare'     => '>',
							'value'       => $detroit_date_now,
							'type'        => 'DATETIME'
					)
				),
				'order'          => 'ASC',
				'orderby'        => 'meta_value',
				'meta_key'       => 'start_time',
				'meta_type'      => 'DATETIME',
			);
			$room = new WP_Query( $args_room );
		?>
		<?php $i = 0; while ( $room->have_posts() ) : $room->the_post(); $i++;?>
			<article>
				<div class="video-wrapper">
					<?php the_field('video_id'); ?>
				</div>
				<p><?php the_title(); ?></p>
			</article>
		<?php endwhile; ?>
	</section>
</main>

<hr class="is-wide">

<aside class="sponsor-aside is-extra-wide">
	<h2 class="is-standard">Click the Sponsor Logos for a virtual introduction and potential promos</h2>
	<?php
		remove_filter( 'pre_get_posts', 'add_my_custom_post_type_to_taxonomy' );
	?>
	<?php //platinum sponsors loop
		$args_platinum_sponsors = array(
			'post_type' => array('sponsor'),
			'posts_per_page' => -1,
			'nopaging' => true,
			'order' => 'DESC',
			'meta_query' => array(
					array(
							'key' => 'level',
							'value' => 'platinum',
					)
			)
		);
		$platinum_sponsors = new WP_Query( $args_platinum_sponsors );
	?>
	<?php //gold sponsors loop
		$args_gold_sponsors = array(
			'post_type' => array('sponsor'),
			'posts_per_page' => -1,
			'nopaging' => true,
			'order' => 'DESC',
			'meta_query' => array(
					array(
							'key' => 'level',
							'value' => 'gold',
					)
			)
		);
		$gold_sponsors = new WP_Query( $args_gold_sponsors );
	?>
	<?php //silver sponsors loop
		$args_silver_sponsors = array(
			'post_type' => array('sponsor'),
			'posts_per_page' => -1,
			'nopaging' => true,
			'order' => 'DESC',
			'meta_query' => array(
					array(
							'key' => 'level',
							'value' => 'silver',
					)
			)
		);
		$silver_sponsors = new WP_Query( $args_silver_sponsors );
	?>
	<?php //bronze sponsors loop
		$args_bronze_sponsors = array(
			'post_type' => array('sponsor'),
			'posts_per_page' => -1,
			'nopaging' => true,
			'order' => 'DESC',
			'meta_query' => array(
					array(
							'key' => 'level',
							'value' => 'bronze',
					)
			)
		);
		$bronze_sponsors = new WP_Query( $args_bronze_sponsors );
	?>
	<?php //other sponsors loop
		$args_other_sponsors = array(
			'post_type' => array('sponsor'),
			'posts_per_page' => -1,
			'nopaging' => true,
			'order' => 'DESC',
			'meta_query' => array(
					array(
							'key' => 'level',
							'value' => 'other',
					)
			)
		);
		$other_sponsors = new WP_Query( $args_other_sponsors );
	?>
	<?php if ( $platinum_sponsors->have_posts() ) : ?>
		<section class="sponsors is-wide">
			<h2>Platinum<br/>Sponsors</h2>
			<div class="sponsor-grid platinum">
				<?php while ( $platinum_sponsors->have_posts() ) : $platinum_sponsors->the_post(); ?>
					<div class="sponsor">
						<a href="<?php the_permalink(); ?>">
							<?php $logo = get_field('logo'); ?>
							<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['large']; ?>" data-srcset="<?php echo $logo['sizes']['small']; ?> 300w, <?php echo $logo['sizes']['medium']; ?> 700w, <?php echo $logo['sizes']['large']; ?> 1000w, <?php echo $logo['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $logo['alt']; ?>">
						</a>
					</div>
				<?php endwhile; ?>
			</div>
		</section>
	<?php endif; wp_reset_postdata(); ?>
	<?php if ( $gold_sponsors->have_posts() ) : ?>
		<section class="sponsors is-wide">
			<h2>Gold<br/>Sponsors</h2>
			<div class="sponsor-grid gold">
				<?php while ( $gold_sponsors->have_posts() ) : $gold_sponsors->the_post(); ?>
					<div class="sponsor">
						<a href="<?php the_permalink(); ?>">
							<?php $logo = get_field('logo'); ?>
							<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['large']; ?>" data-srcset="<?php echo $logo['sizes']['small']; ?> 300w, <?php echo $logo['sizes']['medium']; ?> 700w, <?php echo $logo['sizes']['large']; ?> 1000w, <?php echo $logo['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $logo['alt']; ?>">
						</a>
					</div>
				<?php endwhile; ?>
			</div>
		</section>
	<?php endif; wp_reset_postdata(); ?>
	<?php if ( $silver_sponsors->have_posts() ) : ?>
		<section class="sponsors is-wide">
			<h2>Silver<br/>Sponsors</h2>
			<div class="sponsor-grid silver">
				<?php while ( $silver_sponsors->have_posts() ) : $silver_sponsors->the_post(); ?>
					<div class="sponsor">
						<a href="<?php the_permalink(); ?>">
							<?php $logo = get_field('logo'); ?>
							<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['large']; ?>" data-srcset="<?php echo $logo['sizes']['small']; ?> 300w, <?php echo $logo['sizes']['medium']; ?> 700w, <?php echo $logo['sizes']['large']; ?> 1000w, <?php echo $logo['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $logo['alt']; ?>">
						</a>
					</div>
				<?php endwhile; ?>
			</div>
		</section>
	<?php endif; wp_reset_postdata(); ?>
	<?php if ( $bronze_sponsors->have_posts() ) : ?>
		<section class="sponsors is-wide">
			<h2>Bronze<br/>Sponsors</h2>
			<div class="sponsor-grid bronze">
				<?php while ( $bronze_sponsors->have_posts() ) : $bronze_sponsors->the_post(); ?>
					<div class="sponsor">
						<a href="<?php the_permalink(); ?>">
							<?php $logo = get_field('logo'); ?>
							<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['large']; ?>" data-srcset="<?php echo $logo['sizes']['small']; ?> 300w, <?php echo $logo['sizes']['medium']; ?> 700w, <?php echo $logo['sizes']['large']; ?> 1000w, <?php echo $logo['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $logo['alt']; ?>">
						</a>
					</div>
				<?php endwhile; ?>
			</div>
		</section>
	<?php endif; wp_reset_postdata(); ?>
	<?php if ( $other_sponsors->have_posts() ) : ?>
		<section class="sponsors other is-wide">
			<h2>Additional<br/>Sponsors</h2>
			<div class="sponsor-grid other">
				<?php while ( $other_sponsors->have_posts() ) : $other_sponsors->the_post(); ?>
					<div class="sponsor">
						<a href="<?php the_permalink(); ?>">
							<?php $logo = get_field('logo'); ?>
							<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['large']; ?>" data-srcset="<?php echo $logo['sizes']['small']; ?> 300w, <?php echo $logo['sizes']['medium']; ?> 700w, <?php echo $logo['sizes']['large']; ?> 1000w, <?php echo $logo['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $logo['alt']; ?>">
						</a>
					</div>
				<?php endwhile; ?>
			</div>
		</section>
	<?php endif; wp_reset_postdata(); ?>
</aside>

<aside class="subta-cta">
	<div class="is-wide">
		<div class="poster"></div>
		<div>
			<h2><span>New Series Unlocked</span>Picking the Perfect<br/>Subscription E-Commerce Platform</h2>
			<p>SUBTA is an ever-growing community and by being part of you will gain even more  Exclusive Discounts, Member only Articles, Videos, and Events like this.</p>
			<a target="_blank" href="https://subta.com/show/picking-the-perfect-platform-2021/" class="button is-pink">Watch Now</a>
		</div>
	</div>
</aside>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>