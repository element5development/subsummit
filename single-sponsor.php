<?php 
/*----------------------------------------------------------------*\

	SPONSOR SINGLE POST TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head <?php if ( get_field('header_description') || get_field('header_button_primary') || get_field('header_button_secondary') ): ?>is-tall<?php endif; ?>">
	<div class="is-narrow">
		<a href="https://subsummit.com/room/exhibit-hall/">Back to Exhibit Hall</a>
		<h1><?php the_field('level'); ?> Sponsor: <span><?php the_title(); ?></span></h1>
		<?php $logo = get_field('logo'); ?>
		<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['large']; ?>" data-srcset="<?php echo $logo['sizes']['small']; ?> 300w, <?php echo $logo['sizes']['medium']; ?> 700w, <?php echo $logo['sizes']['large']; ?> 1000w, <?php echo $logo['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $logo['alt']; ?>">
	</div>
</header>

<main id="main-content">
	<?php if( have_rows('article') ):  ?>
		<article>
			<?php get_template_part('template-parts/article'); ?>
		</article>
	<?php else : ?>
		<article>
			<section class="is-narrow">
				<h2>Coming soon.</h2>
			</section>
		</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>