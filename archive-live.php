<?php 
/*----------------------------------------------------------------*\

	DEFAULT CATEGORY ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php // Find current date and time
	$universal_date_now = date('Y-m-d H:i:s');
	$universal_time_now = strtotime($universal_date_now);
	$detroit_time_now = strtotime('-5 hours', $universal_time_now);
	$detroit_date_now = date('Y-m-d H:i:s', $detroit_time_now);
	$detroit_date_now_value = date('YmdHis', $detroit_time_now);
	$future_time_now = strtotime('-4 hours', $universal_time_now);
	$future_date_now = date('Y-m-d H:i:s', $future_time_now);
	$future_date_now_value = date('YmdHis', $future_time_now);
?>
<?php //taxonomy
	$taxonomies = get_terms( array(
		'taxonomy' => 'room',
		'hide_empty' => true
	) );
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<aside class="live-presented-by is-wide">
	<?php $logo = get_field('sponsor_logo', 'option'); ?>
	Live stream sponsored by <img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['large']; ?>" data-srcset="<?php echo $logo['sizes']['small']; ?> 300w, <?php echo $logo['sizes']['medium']; ?> 700w, <?php echo $logo['sizes']['large']; ?> 1000w, <?php echo $logo['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $logo['alt']; ?>">
</aside>

<main id="main-content">
	<?php if ( !empty($taxonomies) ) : ?>
		<section class="room-grid is-wide">
			<?php $i = 0; foreach( $taxonomies as $category ) : $i++; ?>
				<a href="<?php echo get_category_link( $category->term_id ); ?>">
					<article>
						<?php $image = get_field('featured_image', $category->taxonomy . '_' . $category->term_id); ?>
						<h3><?php echo $category->name ?></h3>
						<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
						<?php //ROOM QUERY
							$args_room = array(
								'posts_per_page' => 1,
								'post_type'      => 'live',
								'tax_query' => array(
									array (
											'taxonomy' => 'room',
											'field' => 'id',
											'terms' => $category->term_id,
									)
								),
								'meta_query'     => array(
									array(
											'key'         => 'end_time',
											'compare'     => '>',
											'value'       => $detroit_date_now,
											'type'        => 'DATETIME'
									)
								),
								'order'          => 'ASC',
								'orderby'        => 'meta_value',
								'meta_key'       => 'start_time',
								'meta_type'      => 'DATETIME',
							);
							$room = new WP_Query( $args_room );
						?>
						<?php if ( $room->have_posts() ) : ?>
							<?php while ( $room->have_posts() ) : $room->the_post(); ?>
									<?php if ($i == 1) : ?><div class="featured-label"><?php endif; ?>
									<h4><?php the_title(); ?></h4>
									<?php //date format
										$start_time_value = date("YmdHis", strtotime(get_field('start_time'))); 
										$end_time_value = date("YmdHis", strtotime(get_field('end_time'))); 
									?>
									<?php if ( $start_time_value < $detroit_date_now_value && $end_time_value > $detroit_date_now_value ) : ?>
										<p class="time-tag is-red">Live</p>
									<?php elseif ( $start_time_value > $detroit_date_now_value && $start_time_value < $future_date_now_value && $end_time_value > $detroit_date_now_value ) : ?>
										<p class="time-tag is-pink">Starting Soon</p>
									<?php else : ?>
										<?php $session_start_time = date("g:i A", strtotime(get_field('start_time')));  ?>
										<p class="time-tag is-green">Premiere at <?php echo $session_start_time; ?> CDT</p>
									<?php endif; ?>
									<?php if ($i == 1) : ?></div><?php endif; ?>
							<?php endwhile; ?>
						<?php endif; wp_reset_postdata(); ?>
					</article>
				</a>
			<?php endforeach ?>
		</section>	
	<?php endif; ?>
	<section class="guides">
		<?php $cover = get_field('program_guide_cover', 'option'); ?>
		<a href="<?php the_field('program_guide', 'option'); ?>">
			<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $cover['sizes']['placeholder']; ?>" data-src="<?php echo $cover['sizes']['large']; ?>" data-srcset="<?php echo $cover['sizes']['small']; ?> 300w, <?php echo $cover['sizes']['medium']; ?> 700w, <?php echo $cover['sizes']['large']; ?> 1000w, <?php echo $cover['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $cover['alt']; ?>">	
		</a>
		<a href="<?php the_field('program_guide', 'option'); ?>" class="button is-purple">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19 9h-4V3H9v6H5l7 7 7-7zM5 18v2h14v-2H5z" fill="currentColor"/></svg>
			Program Guide
		</a>
		<a href="<?php the_field('buyer_guide', 'option'); ?>" class="button is-purple">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19 9h-4V3H9v6H5l7 7 7-7zM5 18v2h14v-2H5z" fill="currentColor"/></svg>
			Buyer's Guide
		</a>
		<?php $cover = get_field('buyer_guide_cover', 'option'); ?>
		<a href="<?php the_field('buyer_guide', 'option'); ?>">
			<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $cover['sizes']['placeholder']; ?>" data-src="<?php echo $cover['sizes']['large']; ?>" data-srcset="<?php echo $cover['sizes']['small']; ?> 300w, <?php echo $cover['sizes']['medium']; ?> 700w, <?php echo $cover['sizes']['large']; ?> 1000w, <?php echo $cover['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $cover['alt']; ?>">	
		</a>
	</section>
</main>

<aside class="live-presented is-wide">
	<?php $logo = get_field('sponsor_banner', 'option'); ?>
	<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['large']; ?>" data-srcset="<?php echo $logo['sizes']['small']; ?> 300w, <?php echo $logo['sizes']['medium']; ?> 700w, <?php echo $logo['sizes']['large']; ?> 1000w, <?php echo $logo['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $logo['alt']; ?>">
</aside>

<aside class="subta-cta">
	<div class="is-wide">
		<div class="poster"></div>
		<div>
			<h2><span>New Series Unlocked</span>Picking the Perfect<br/>Subscription E-Commerce Platform</h2>
			<p>SUBTA is an ever-growing community and by being part of you will gain even more  Exclusive Discounts, Member only Articles, Videos, and Events like this.</p>
			<a target="_blank" href="https://subta.com/show/picking-the-perfect-platform-2021/" class="button is-pink">Watch Now</a>
		</div>
	</div>
</aside>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>