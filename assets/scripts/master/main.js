var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
		FORM FUNCTIONS
	\*----------------------------------------------------------------*/
	/*---------------------------*\
		SELECT FIELD PLACEHOLDER
	\*---------------------------*/
	$(function () {
		$('select').addClass('has-placeholder');
	});
	$("select").change(function () {
		$(this).removeClass('has-placeholder');
	});
	/*---------------------------*\
		FILE UPLOAD
	\*---------------------------*/
	$('label').each(function () {
		if ($(this).siblings('.ginput_container_fileupload').length) {
			$(this).addClass('file-upload-label');
		}
	});

	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			$('label.file-upload-label').addClass("file-uploaded");
		});
	}
	/*----------------------------------------------------------------*\
		IFRAME LAZYLOAD
	\*----------------------------------------------------------------*/
	$(function () {
		$('iframe').addClass('lazyload');
	});
	/*----------------------------------------------------------------*\
		NOTIFICATION BAR
	\*----------------------------------------------------------------*/
	if (readCookie('cookieNotification') === 'false') {
		$('.cookie-useage-notification').removeClass("note-on");
	} else {
		$('.cookie-useage-notification').addClass("note-on");
	}
	$('.cookie-useage-notification button').click(function () {
		$('.cookie-useage-notification').removeClass("note-on");
		createCookie('cookieNotification', 'false');
	});

	function createCookie(name, value, days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + value + expires + "; path=/";
	}

	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
		}
		return null;
	}
	/*----------------------------------------------------------------*\
		FIXED NAV
	\*----------------------------------------------------------------*/
	var navHeight = $('.primary-nav-wrap').height() + 24;
	var halfVH  =$(window).height() / 2;
	var lastScrollTop = 0;
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();
		if (scroll >= halfVH) {
			$('html').css('padding-top', navHeight);
			var scrollTop = $(this).scrollTop();
			if (scrollTop > lastScrollTop){
				$('.primary-nav-wrap').addClass('is-fixed').css('top', - navHeight);
			} else {
				$('.primary-nav-wrap').addClass('is-fixed').css('top', '0px');
			}
			lastScrollTop = scrollTop;
		} else if (scroll < halfVH && scroll > navHeight) {
			$('.primary-nav-wrap').css('top', - navHeight);
		} else {
			$('.primary-nav-wrap').removeClass('is-fixed').css('top', 'auto');
			$('html').css('padding-top', '0');
		}
	});
	/*----------------------------------------------------------------*\
		MOBILE MENU TOGGLE
	\*----------------------------------------------------------------*/
	$(".mobile-menu-toggle").click(function(){
		$("nav.mobile-menu").toggleClass("is-active");
	});
	/*----------------------------------------------------------------*\
		SPONSOR SLIDER
	\*----------------------------------------------------------------*/
	$('.sponsor-slider .sponsors').slick({
		infinite: true,
		slidesPerRow: 5,
    rows: 2,
		pauseOnHover: false,
		dots: false,
		arrows: true,
		autoplay: true,
		autoplaySpeed: 2000,
		responsive: [{
				breakpoint: 800,
				settings: {
					slidesPerRow: 3,
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesPerRow: 2,
				}
			}
		]
	});
	/*----------------------------------------------------------------*\
		SCHEDULE TABS
	\*----------------------------------------------------------------*/
	$("nav.tabs button.day1").click(function () {
		$('nav.tabs button').removeClass("is-active");
		$('section.schedule').removeClass("is-active");
		$(this).addClass("is-active");
		$('section.schedule.day1').addClass("is-active");
	});
	$("nav.tabs button.day2").click(function () {
		$('nav.tabs button').removeClass("is-active");
		$('section.schedule').removeClass("is-active");
		$(this).addClass("is-active");
		$('section.schedule.day2').addClass("is-active");
	});
	$("nav.tabs button.day3").click(function () {
		$('nav.tabs button').removeClass("is-active");
		$('section.schedule').removeClass("is-active");
		$(this).addClass("is-active");
		$('section.schedule.day3').addClass("is-active");
	});
	/*----------------------------------------------------------------*\
		TESTIMONAIL SLIDER
	\*----------------------------------------------------------------*/
	$('.testimonial-slider .testimonials').slick({
		infinite: true,
		pauseOnHover: false,
		slidesToShow: 1,
  	slidesToScroll: 1,
		dots: false,
		arrows: true,
		autoplay: true,
		autoplaySpeed: 5000,
	});
	/*----------------------------------------------------------------*\
		INITIATE MICRO MODAL
	\*----------------------------------------------------------------*/
	MicroModal.show('snack-modal');
});