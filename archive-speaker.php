<?php 
/*----------------------------------------------------------------*\

	DEFAULT CATEGORY ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php 
	$post_type = get_query_var('post_type'); 
	if ( $post_type == '' ) {
		$post_type = 'post';
	}
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head <?php echo $class; ?>">
	<div class="is-standard">
		<h1>
			<?php if ( get_field('speaker_preheader','option') ) : ?>
				<span><?php the_field('speaker_preheader','option'); ?></span>
			<?php endif; ?>
			<?php the_field('speaker_title','option'); ?>
		</h1>
		<?php if( get_field('speaker_button','option') ): ?>
			<?php 
				$link = get_field('speaker_button','option');
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
			?>
			<a class="button is-pink" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
		<?php endif; ?>
	</div>
</header>

<main id="main-content">
	<article>
		<?php if (have_posts()) : ?>
			<section class="speaker-grid is-extra-wide">
				<?php	while ( have_posts() ) : the_post(); ?>
				<article class="single-speaker">
					<?php $headshot = get_field('headshot'); ?>
					<div class="headshot">
						<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $headshot['sizes']['placeholder']; ?>" data-src="<?php echo $headshot['sizes']['small']; ?>" alt="<?php echo $headshot['alt']; ?>" />
					</div>
					<?php $logo = get_field('company_logo'); ?>
					<div class="company-logo">
						<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $logo['sizes']['largeplaceholder']; ?>" data-src="<?php echo $logo['sizes']['small']; ?>" alt="<?php echo $logo['alt']; ?>" />
					</div>
					<h4><?php echo get_the_title(); ?></h4>
					<p><small><?php the_field('title'); ?></small></p>
				</article>
				<?php endwhile; ?>
			</section>
		<?php else : ?>
			<article>
				<section class="is-narrow">
					<p>Uh Oh. Something is missing. Looks like this page has no content.</p>
				</section>
			</article>
		<?php endif; ?>
	</article>
	<?php clean_pagination(); ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>